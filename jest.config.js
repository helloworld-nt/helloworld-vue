module.exports = {
  testMatch: ['<rootDir>/src/**/*.test.js'],
  //  collectCoverage: true,
  coverageDirectory: '<rootDir>/generated/coverage',
  verbose: true,
};
