// eslint-disable-next-line import/no-extraneous-dependencies
const path = require('path');

const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin');

module.exports = {
  configureWebpack: {
    plugins: [new VuetifyLoaderPlugin()],
    resolve: {
      alias: {
        '~': path.resolve(__dirname, 'src'),
      },
    },
  },
  devServer: {
    port: 4000,
  },
};
