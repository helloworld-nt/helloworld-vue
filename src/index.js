import './common/init';

import './styles/index.css';

import Vue from 'vue';

import './common/vue';

import vuetify from './common/vuetify';
import './common/portal';

import './components/shared';

import store from './store';

import App from './App.vue';

// import './registerServiceWorker';

const app = new Vue({
  store,
  vuetify,
  render: (h) => h(App),
});

app.$mount('#app');

if (process.env.NODE_ENV === 'development') {
  globalThis.$app = app;
}

// import('pwacompat');
