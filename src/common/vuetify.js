import 'material-design-icons/iconfont/material-icons.css';

import Vue from 'vue';

import Vuetify from 'vuetify/lib';

import { COLOR } from '~/styles';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'md',
  },
  theme: {
    dark: false,
    themes: {
      light: {
        primary: COLOR.primary,
        secondary: COLOR.primaryDark,
        accent: COLOR.accent,
        inverse: COLOR.inverse,
        error: COLOR.error,
        success: COLOR.success,
        info: COLOR.info,
        warning: COLOR.warning,
        off: COLOR.off,
      },
    },
  },
});
