import { AuthService } from './service';

import * as Activity from '../Shared/Activity';

/* eslint no-param-reassign: ["error", { "props": false }] */

const MODULE = 'Auth';

const state = {
  namespaced: true,

  state: {
    authenticated: false,
    userAccount: null,
    userProfile: null,
  },

  mutations: {
    authenticate: (state, { userAccount = null, userProfile = null }) => {
      state.userAccount = userAccount;
      state.userProfile = userProfile;
      state.authenticated = true;
    },

    deauthenticate: (state) => {
      state.userAccount = null;
      state.userProfile = null;
      state.authenticated = false;
    },
  },

  actions: {
    login: (context, { username, password }) => {
      Activity.processing(MODULE, 'login');
      return AuthService.login(username, password)
        .then(({ userAccount, userProfile }) => {
          context.commit('authenticate', { userAccount, userProfile });
          return { userAccount, userProfile };
        })
        .finally(() => Activity.done(MODULE, 'login'));
    },

    signup: (context, { name, email, password }) => {
      Activity.processing(MODULE, 'signup');
      return AuthService.signup(name, email, password)
        .then(({ userAccount, userProfile }) => {
          context.commit('authenticate', { userAccount, userProfile });
          return { userAccount, userProfile };
        })
        .finally(() => Activity.done(MODULE, 'signup'));
    },

    initiatePasswordReset: (context, { email }) => {
      Activity.processing(MODULE, 'initiatePasswordReset');
      return AuthService.initiatePasswordReset(email).finally(() => Activity.done(MODULE, 'initiatePasswordReset'));
    },

    logout: (context) => {
      Activity.processing(MODULE, 'logout');
      return AuthService.logout()
        .then(() => {
          context.commit('deauthenticate');
          context.commit('resetDrawers');
        })
        .finally(() => Activity.done(MODULE, 'logout'));
    },
  },

  getters: {},
};

export default state;
