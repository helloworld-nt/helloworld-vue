import { EventEmitter } from '../common/events';

import { API_ENDPOINT } from '../common/config';
import * as FetchHelper from '../common/fetch.helper';

export const AuthServiceImpl = class AuthService {
  events = new EventEmitter();

  username = '';

  password = '';

  accessToken = null;

  getAccessToken() {
    return this.accessToken;
  }

  async _loadSession() {
    this.accessToken = localStorage.getItem('auth.accessToken') || null;
  }

  async _saveSession(accessToken) {
    this.accessToken = accessToken || null;
    localStorage.setItem('auth.accessToken', this.accessToken);
  }

  async _clearSession() {
    this.accessToken = null;
    localStorage.removeItem('auth.accessToken');
  }

  async initialize() {
    await this._loadSession();
  }

  isAuthenticated() {
    return !!this.accessToken;
  }

  login(username, password) {
    return FetchHelper.handle(
      fetch(`${API_ENDPOINT}/auth/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username,
          password,
        }),
      }),
    ).then(async ({ content: { accessToken, ...content } }) => {
      await this._saveSession(accessToken);
      await this.events.emitAsync('login');
      return content;
    });
  }

  async logout() {
    await this.events.emitAsync('logout');
    await this._clearSession();
  }

  signup(name, email, password) {
    const userAccount = {
      name,
      email,
      password,
    };

    return FetchHelper.handle(
      fetch(`${API_ENDPOINT}/auth/signup`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          userAccount,
          userProfile: {},
        }),
      }),
    ).then(({ content: { accessToken, ...content } }) => {
      this._saveSession(accessToken);
      return content;
    });
  }

  initiatePasswordReset(email) {
    return fetch(`${API_ENDPOINT}/auth/password-reset/initiate`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email,
      }),
    }).then(({ content }) => content);
  }
};

export const AuthService = new AuthServiceImpl();

if (process.env.NODE_ENV === 'development') {
  globalThis.AuthService = AuthService;
}
