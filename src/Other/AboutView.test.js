import { shallowMount } from '@vue/test-utils';
import AboutView from './AboutView.vue';

describe('AboutView', () => {
  test('renders correctly', () => {
    const msg = 'This is an about page';

    const wrapper = shallowMount(AboutView, {
      propsData: { msg },
    });

    expect(wrapper.text()).toMatch(msg);
  });
});
