import Vue from 'vue';

import XSnackbar from '~/components/XSnackbar.vue';

Vue.component('XSnackbar', XSnackbar);
