import { AuthService } from '../Auth/service';

import { API_ENDPOINT } from '../common/config';

import * as FetchHelper from '../common/fetch.helper';

import * as Activity from '../Shared/Activity';

/* eslint no-param-reassign: ["error", { "props": false }] */

const MODULE = 'Home';

const state = {
  namespaced: true,

  state: {
    post: null,
  },

  mutations: {
    post: (state, value = null) => {
      state.post = value;
    },
  },

  actions: {
    'post.fetch': (context) => {
      Activity.processing(MODULE, 'operation');

      return FetchHelper.handle(
        fetch(`${API_ENDPOINT}/user/post/collection`, {
          headers: {
            Authorization: `Bearer ${AuthService.getAccessToken()}`,
          },
        }),
      )
        .then(async ({ content }) => {
          context.commit('post', content.data);
          return content.data;
        })
        .finally(() => Activity.done(MODULE, 'operation'));
    },

    'post.create': (context, post) => {
      Activity.processing(MODULE, 'operation');

      return FetchHelper.handle(
        fetch(`${API_ENDPOINT}/ask/create`, {
          method: 'POST',
          headers: {
            Authorization: `Bearer ${AuthService.getAccessToken()}`,
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            post,
          }),
        }),
      )
        .then(async ({ content }) => content.post)
        .finally(() => Activity.done(MODULE, 'operation'));
    },

    'post.edit': (context, post) => {
      Activity.processing(MODULE, 'operation');

      return FetchHelper.handle(
        fetch(`${API_ENDPOINT}/user/post/${post.id}/edit`, {
          method: 'POST',
          headers: {
            Authorization: `Bearer ${AuthService.getAccessToken()}`,
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            post,
          }),
        }),
      )
        .then(async ({ content }) => content.post)
        .finally(() => Activity.done(MODULE, 'operation'));
    },

    'post.remove': (context, postId) => {
      Activity.processing(MODULE, 'operation');

      return FetchHelper.handle(
        fetch(`${API_ENDPOINT}/user/post/${postId}/delete`, {
          method: 'POST',
          headers: {
            Authorization: `Bearer ${AuthService.getAccessToken()}`,
          },
        }),
      )
        .then(async ({ content }) => content.post)
        .finally(() => Activity.done(MODULE, 'operation'));
    },
  },

  getters: {},
};

export default state;
