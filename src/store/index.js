import Vue from 'vue';
import Vuex from 'vuex';

import createPersistedState from 'vuex-persistedstate';

import { Logger } from '../common/logger';

import modules from './state';

Vue.use(Vuex);

/* eslint no-param-reassign: ["error", { "props": false }] */

const store = new Vuex.Store({
  strict: process.env.NODE_ENV === 'development',

  plugins: [
    createPersistedState({
      // @TODO use persister() function in module state definition
      reducer({ route, Shared, Activity, navigationDrawerVisible, detailsDrawerVisible, ...state }) {
        return state;
      },
    }),
  ],

  modules,

  state: {
    navigationDrawerVisible: false,
    detailsDrawerVisible: false,
  },

  mutations: {
    navigationDrawerVisible: (state, value = false) => {
      state.navigationDrawerVisible = value;
    },

    detailsDrawerVisible: (state, value = false) => {
      state.detailsDrawerVisible = value;
    },

    // resetDrawers: (state) => {
    //   state.navigationDrawerVisible = false;
    //   state.detailsDrawerVisible = false;
    // },
  },

  actions: {
    'NavigationDrawer.show': (context) => {
      Logger.debug('NavigationDrawer.show', context);
      context.commit('navigationDrawerVisible', true);
    },

    'NavigationDrawer.hide': (context) => {
      Logger.debug('NavigationDrawer.show', context);
      context.commit('navigationDrawerVisible');
    },

    'DetailsDrawer.show': (context) => {
      Logger.debug('DetailsDrawer.show', context);
      context.commit('detailsDrawerVisible', true);
    },

    'DetailsDrawer.hide': (context) => {
      Logger.debug('DetailsDrawer.show', context);
      context.commit('detailsDrawerVisible');
    },
  },

  getters: {
    developmentMode: () => process.env.NODE_ENV === 'development',
  },
});

export default store;
