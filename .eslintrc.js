/* eslint-disable no-undef, global-require, import/no-extraneous-dependencies */

module.exports = {
  root: true,
  extends: [
    'eslint:recommended',
    'airbnb-base',
    'plugin:vue/recommended',
    'plugin:prettier/recommended',
    'prettier/vue',
  ],
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 2020,
    sourceType: 'module',
  },
  env: {
    es2020: true,
    browser: true,
    node: true,
  },
  globals: {
    globalThis: 'readonly',
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'max-len': ['warn', { code: 120 }],
    'arrow-parens': ['warn', 'always'],
    'arrow-body-style': 'off',
    camelcase: 'warn',
    'no-underscore-dangle': 'warn',
    'no-param-reassign': 'warn',
    'no-unused-vars': 'warn',
    'class-methods-use-this': 'warn',
    'prefer-destructuring': 'warn',
    'prefer-const': 'warn',
    'no-shadow': 'warn',
    'no-empty': 'warn',
    'no-restricted-syntax': require('eslint-config-airbnb-base/rules/style').rules['no-restricted-syntax'].filter(
      (item) => typeof item !== 'object' || item.selector !== 'ForOfStatement',
    ),
    'import/prefer-default-export': 'off',
    'import/no-cycle': 'warn',
    'vue/attributes-order': 'warn',
    'vue/require-default-prop': 'warn',
    'vue/camelcase': 'error',
    'vue/component-name-in-template-casing': [
      'error',
      'PascalCase',
      {
        registeredComponentsOnly: false,
      },
    ],
  },
  overrides: [
    {
      files: ['*.test.js', 'tests/**/*.js'],
      env: {
        jest: true,
      },
    },
  ],
  settings: {
    'import/resolver': {
      webpack: {
        config: require.resolve('@vue/cli-service/webpack.config.js'),
      },
    },
  },
};
